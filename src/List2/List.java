package List2;

public class List {
    private Node first;
    private Node last;

    public List() {
    }

    public boolean isEmpty() {
        return first == null;
    }

    public void pushBack(String value) {
        Node node = new Node(value);
        if (isEmpty()) {
            first = node;
            last = node;
            return;
        }
        last.setNext(node);
        last = node;
    }

    public int findSize() {
        int count = 0;
        Node current = first;
        while (current != last) {
            count++;
            current = current.getNext();
        }
        count++;
        return count;
    }

    public String get(int index) {
        if (index < 0 || index > findSize()) {
            throw new RuntimeException("Index is invalid.");
        }
        Node current = first;
        for (int i = 0; i < index; i++) {
            current = current.getNext();
        }
        return current.getValue();
    }

    public Node getNode(int index) {
        if (index < 0 || index > findSize()) {
            throw new RuntimeException("Index is invalid.");
        }
        Node current = first;
        for (int i = 0; i < index; i++) {
            current = current.getNext();
        }
        return current;
    }

    public void insert(int index, String item) {
        if (index < 0 || index > findSize()) {
            throw new RuntimeException("Index is invalid.");
        }
        Node newNode = new Node(item);
        Node current = first;
        for (int i = 1; i < index; i++) {
            current = current.getNext();
        }
        newNode.setNext(current.getNext());
        current.setNext(newNode);
    }

    public void replace(int index, String item) {
        if (index < 0 || index > findSize()) {
            throw new RuntimeException("Index is invalid.");
        }
        Node newNode = this.getNode(index);
        newNode.setValue(item);
    }

    public void delete(int index) {
        if (index < 0 || index > findSize()) {
            throw new RuntimeException("Index is invalid.");
        }
        Node current = this.getNode(index - 1);
        if (this.getNode(index) != last) {
            Node next = current.getNext();
            current.setNext(next.getNext());
            next.setNext(null);
        } else {
            current.setNext(null);
            this.last = current;
        }
    }
}